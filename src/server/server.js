const express = require('express');
const fetch = require('node-fetch');
const proxy = require('express-http-proxy');
const color = require('./color-codes')
let fs = require('fs');

const app = express();
const port = 3000;

// Lokal override om man inte kör docker-compose
const staticaddress = process.argv[2] || 'build';
const elasticaddress = process.argv[3] || 'http://localhost:9200';
const mappingFilePath = process.argv[4] || 'ElasticsearchConfigs/elasticMappings.txt';
const testDataFilePath = process.argv[5] || 'ElasticsearchConfigs/test_data.json';

async function createMapping() {
    const mapping = fs.readFileSync(mappingFilePath);
    return fetch(elasticaddress + "/test_data?pretty", {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: mapping
    });
}

async function importData() {
    const    testData = fs.createReadStream(testDataFilePath);
    return fetch(elasticaddress + "/test_data/googleVision/_bulk?pretty", {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: testData
    });
}

(async function dataSetup() {
    let response;
    while(!response) {
        try {
            response = await fetch(elasticaddress + "/test_data?pretty", {
                method: 'GET',
                headers: { 'Content-Type': 'application/json' }
            })
            response = await response.json();
            console.log("Ansluten till db: " + elasticaddress)
        } catch {
            console.log("Kan inte ansluta till db: " + elasticaddress + "\n" + "Försöker igen...")
            var dt = new Date();
            while ((new Date()) - dt <= 2000) {}
        }
    }

    if(response.error) {
        await createMapping().then((res) => console.log("Create mapping: " + color.FgGreen + res.statusText + color.Reset));
        await importData().then((res) => console.log("Import data: " + color.FgGreen + res.statusText + color.Reset));
    }
})()

app.use('/test_data', proxy(elasticaddress, {
    proxyReqPathResolver: function (req, res) {
        return '/test_data' + req.url
    }
}))

app.use('/', express.static(staticaddress));

app.listen(port, function() {
    console.log('Express server listening on port ' + port);
});