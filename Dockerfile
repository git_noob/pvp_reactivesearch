FROM node:10

RUN mkdir /www /server
RUN cd /server && npm install express express-http-proxy node-fetch shelljs

COPY build /www
COPY src/server/*.* /server/
COPY ElasticsearchConfigs/*.* /server/

WORKDIR /server
CMD node "server.js" "/www" "http://elasticsearch:9200" "/server/elasticMappings.txt" "/server/test_data.json"